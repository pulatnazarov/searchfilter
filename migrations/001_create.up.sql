create table cars (
    id uuid not null primary key,
    category varchar(6) not null,
    name varchar(20) not null,
    model varchar(20) not null,
    color varchar(30) not null,
    year int not null,
    price int not null,
    distance int not null
);

create table houses (
                              id uuid not null primary key,
                              category varchar(6) not null,
                              class varchar(20) not null,
                              location varchar(40) not null,
                              year int not null,
                              price int not null,
                              square int not null
);

create table jobs (
    id uuid not null primary key,
    category varchar(6) not null,
    class varchar(20) not null,
    location varchar(40) not null,
    salary int not null,
    hours int not null
    );
