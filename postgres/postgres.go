package postgres

import (
	"context"
	"database/sql"
	"errors"
	"filter/entity"
	"fmt"
	"log"
)

type Postgres struct {
	db *sql.DB
}

func New(db *sql.DB) Postgres {
	return Postgres{
		db: db,
	}
}

type PostgresRepo interface {
	Filter(ctx context.Context, category, name, model, class, location, color string,
		year, price, square, salary, distance, hours int) (entity.Filter, error)

	SelectHouses(ctx context.Context) ([]entity.House, error)
	SelectHouse(ctx context.Context, houseId string) (entity.House, error)
	InsertHouse(ctx context.Context, house entity.House) error

	UpdateCar(ctx context.Context, newCar entity.Car) error
	SelectCars(ctx context.Context) ([]entity.Car, error)
	SelectCar(ctx context.Context, carId string) (entity.Car, error)
	InsertCar(ctx context.Context, car entity.Car) error
}

func (p Postgres) Filter(ctx context.Context, category, name, model, class, location, color string,
	year, price, square, salary, distance, hours int) (entity.Filter, error) {
	var (
		cars   []entity.Car
		houses []entity.House
		jobs   []entity.Job
	)
	if category != "" {
		if category == "cars" {
			query := fmt.Sprintf("select * from %s where category = 'Car' ", category)
			if name != "" {
				query += fmt.Sprintf(`and name like '%%%s%%' `, name)
			}
			if model != "" {
				query += fmt.Sprintf(`and model like '%%%s%%' `, model)
			}
			if color != "" {
				query += fmt.Sprintf(`and color like '%%%s%%' `, color)
			}
			if year != 0 {
				query += fmt.Sprintf(`and year = %d `, year)
			}
			if price != 0 {
				query += fmt.Sprintf(`and price = %d `, price)
			}
			if distance != 0 {
				query += fmt.Sprintf(`and distance = %d `, distance)
			}
			rows, err := p.db.QueryContext(ctx, query)
			if err != nil {
				return entity.Filter{}, err
			}
			for rows.Next() {
				car := entity.Car{}
				if err = rows.Scan(&car.Id, &car.Category, &car.Name, &car.Model, &car.Color, &car.Year, &car.Price, &car.Distance); err != nil {
					log.Printf("{%v}", err)
					return entity.Filter{}, err
				}
				cars = append(cars, car)
			}
			log.Println(cars)
		} else if category == "houses" {
			query := fmt.Sprintf("select * from %s where category = 'House' ", category)
			if class != "" {
				query += fmt.Sprintf("and class like '%%%s%%' ", class)
			}
			if location != "" {
				query += fmt.Sprintf("and location like '%%%s%%' ", location)
			}
			if square != 0 {
				query += fmt.Sprintf("and square = %d ", square)
			}
			if year != 0 {
				query += fmt.Sprintf("and year = %d ", year)
			}
			if price != 0 {
				query += fmt.Sprintf("and price = %d ", price)
			}
			rows, err := p.db.QueryContext(ctx, query)
			if err != nil {
				return entity.Filter{}, err
			}
			for rows.Next() {
				house := entity.House{}
				if err = rows.Scan(&house.Id, &house.Category, &house.Class, &house.Location, &house.Year, &house.Price, &house.Square); err != nil {
					return entity.Filter{}, err
				}
				houses = append(houses, house)
			}
		} else if category == "jobs" {
			query := fmt.Sprintf("select * from %s where category = 'Job' ", category)
			if class != "" {
				query += fmt.Sprintf("and class like %%%s%% ", class)
			}
			if salary != 0 {
				query += fmt.Sprintf("and salary = %d ", salary)
			}
			if location != "" {
				query += fmt.Sprintf("and location like %%%s%% ", location)
			}
			if hours != 0 {
				query += fmt.Sprintf("and hours = %d ", hours)
			}
			rows, err := p.db.QueryContext(ctx, query)
			if err != nil {
				return entity.Filter{}, err
			}
			for rows.Next() {
				job := entity.Job{}
				if err = rows.Scan(&job.Id, &job.Category, &job.Class, &job.Location, &job.Salary, &job.Hours); err != nil {
					return entity.Filter{}, err
				}
				jobs = append(jobs, job)
			}
		}
		return entity.Filter{
			Cars:   cars,
			Houses: houses,
			Jobs:   jobs,
		}, nil
	}
	return entity.Filter{}, nil
}

func (p Postgres) SelectHouses(ctx context.Context) ([]entity.House, error) {
	houses := []entity.House{}
	rows, err := p.db.QueryContext(ctx, `select * from houses`)
	if err != nil {
		log.Println("2: ", err)
		return nil, err
	}
	for rows.Next() {
		house := entity.House{}
		if err = rows.Scan(&house.Id, &house.Category, &house.Class, &house.Location, &house.Year, &house.Price, &house.Square); err != nil {
			log.Println("3: ", err)
			return nil, err
		}
		houses = append(houses, house)
	}

	return houses, nil
}

func (p Postgres) SelectHouse(ctx context.Context, houseId string) (entity.House, error) {
	house := entity.House{}
	if err := p.db.QueryRowContext(ctx, `select * from houses where id = $1`, houseId).Scan(&house.Id, &house.Category, &house.Class, &house.Location, &house.Year, &house.Price, &house.Square); err != nil {
		return entity.House{}, err
	}
	return house, nil
}

func (p Postgres) InsertHouse(ctx context.Context, house entity.House) error {
	_, err := p.db.ExecContext(ctx, `insert into houses values ($1, $2, $3, $4, $5, $6, $7)`, house.Id, house.Category, house.Class, house.Location, house.Year, house.Price, house.Square)
	return err
}

func (p Postgres) UpdateCar(ctx context.Context, newCar entity.Car) error {
	query := fmt.Sprintf(`update cars set id = '%s' `, newCar.Id)
	if newCar.Category == "Car" {
		if newCar.Name != "" {
			query += fmt.Sprintf(", name = '%s' ", newCar.Name)
		}
		if newCar.Model != "" {
			query += fmt.Sprintf(", model = '%s' ", newCar.Model)
		}
		if newCar.Color != "" {
			log.Println(newCar.Color)
			query += fmt.Sprintf(", color = '%s' ", newCar.Color)
		}
		if newCar.Year != 0 {
			query += fmt.Sprintf(", year = %d ", newCar.Year)
		}
		if newCar.Price != 0 {
			query += fmt.Sprintf(", price = %d ", newCar.Price)
		}
		if newCar.Distance != 0 {
			query += fmt.Sprintf(", distance = %d", newCar.Distance)
		}
		query += fmt.Sprintf("where id = '%s';", newCar.Id)
		log.Println("query:  ", query)
		if _, err := p.db.ExecContext(ctx, query); err != nil {
			log.Println("3: ", err)
			return err
		}
		return nil
	}
	return errors.New("category is not car")
}

func (p Postgres) SelectCars(ctx context.Context) ([]entity.Car, error) {
	cars := []entity.Car{}

	rows, err := p.db.QueryContext(ctx, `select * from cars`)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		car := entity.Car{}
		if err = rows.Scan(&car.Id, &car.Category, &car.Name, &car.Model, &car.Color, &car.Year, &car.Price, &car.Distance); err != nil {
			return nil, err
		}
		cars = append(cars, car)
	}
	return cars, nil
}

func (p Postgres) SelectCar(ctx context.Context, carId string) (entity.Car, error) {
	var car entity.Car
	fmt.Println(carId)
	if err := p.db.QueryRowContext(ctx, `select * from cars where id=$1`, carId).Scan(&car.Id, &car.Category, &car.Name, &car.Model, &car.Color, &car.Year, &car.Price, &car.Distance); err != nil {
		log.Println("2: ", err)
		return entity.Car{}, err
	}
	return car, nil
}

func (p Postgres) InsertCar(ctx context.Context, car entity.Car) error {
	if car.Category != "Car" {
		return errors.New("invalid category")
	}
	if _, err := p.db.ExecContext(ctx, `insert into cars values ($1, $2, $3, $4, $5, $6, $7, $8)`, car.Id, car.Category, car.Name, car.Model, car.Color, car.Year, car.Price, car.Distance); err != nil {
		return err
	}
	return nil
}
