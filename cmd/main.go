package main

import (
	"filter/api"
	"filter/config"
	"filter/connect"
	"filter/postgres"
	"filter/service"
	"log"
)

func main() {
	cfg := config.Load()

	db, err := connect.Connect(cfg)
	if err != nil {
		log.Fatalln(err)
	}

	repo := postgres.New(db)

	s := service.New(repo)

	api.NewRouter(cfg, s)
}
