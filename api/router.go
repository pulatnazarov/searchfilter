package api

import (
	"filter/config"
	_ "filter/docs"
	"filter/service"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"net"
)

// @title           Postgres Crud API
// @version         1.0
// @description     This is a sample server celler server.

// NewRouter
// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io
func NewRouter(cfg config.Config, service service.Service) {
	r := gin.Default()

	s := Server{
		service: service,
	}

	r.GET("/filter", s.Filter)

	r.GET("/houses", s.GetHouses)
	r.GET("/house", s.GetHouse)
	r.POST("/register/house", s.RegisterHouse)

	r.PUT("/update/car", s.UpdateCar)
	r.GET("/cars", s.GetCars)
	r.GET("/car/:carId", s.GetCar)
	r.POST("/register/car", s.RegisterCar)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.Run(net.JoinHostPort(cfg.Host, cfg.Port))
}
