package swagger_object

type Car struct {
	Category string `json:"category" binding:"required"`
	Name     string `json:"name"`
	Model    string `json:"model"`
	Color    string `json:"color"`
	Year     int    `json:"year"`
	Price    int    `json:"price"`
	Distance int    `json:"distance"`
}

type House struct {
	Category string `json:"category" binding:"required"`
	Class    string `json:"class"`
	Location string `json:"location"`
	Year     int    `json:"year"`
	Price    int    `json:"price"`
	Square   int    `json:"square"`
}

type Job struct {
	Category string `json:"category" binding:"required"`
	Class    string `json:"class"`
	Location string `json:"location"`
	Salary   int    `json:"salary"`
	Hours    int    `json:"hours"`
}
