package api

import (
	"context"
	"filter/api/swagger_object"
	"filter/entity"
	"filter/service"
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"strconv"
)

/* @Param        currency query string false "currency" Enums(UZS, USD)*/
type Server struct {
	service service.ServiceRepo
}

// Filter
// @Summary      Filter
// @Description
// @Tags         filter
// @Accept       json
// @Produce      json
// @Param        category query string false "category" Enums(car, house, job)
// @Param        name query string false "name"
// @Param        model query string false "model"
// @Param        class query string false "class"
// @Param        location query string false "location"
// @Param        color query string false "color"
// @Param        year query string false "year"
// @Param        price query string false "price"
// @Param        square query string false "square"
// @Param        salary query string false "salary"
// @Param        distance query string false "distance"
// @Param        hours query string false "hours"
// @Success      200 {object} entity.Filter
// @Failure      400
// @Failure      500
// @Router       /filter [GET]
func (s Server) Filter(c *gin.Context) {
	var (
		err                                          error
		year, distance, price, square, salary, hours int
	)
	category := c.Query("category")
	category += "s"
	name := c.Query("name")
	model := c.Query("model")
	class := c.Query("class")
	location := c.Query("location")
	color := c.Query("color")
	if c.Query("year") != "" {
		year, err = strconv.Atoi(c.Query("year"))
		if invalidInput(c, err) {
			return
		}
	}
	if c.Query("price") != "" {
		price, err = strconv.Atoi(c.Query("price"))
		if invalidInput(c, err) {
			return
		}
	}
	if c.Query("square") != "" {
		square, err = strconv.Atoi(c.Query("square"))
		if invalidInput(c, err) {
			return
		}
	}
	if c.Query("salary") != "" {
		salary, err = strconv.Atoi(c.Query("salary"))
		if invalidInput(c, err) {
			return
		}
	}
	if c.Query("distance") != "" {
		distance, err = strconv.Atoi(c.Query("distance"))
		if invalidInput(c, err) {
			return
		}
	}
	if c.Query("hours") != "" {
		hours, err = strconv.Atoi(c.Query("hours"))
		if invalidInput(c, err) {
			return
		}
	}
	f, err := s.service.Filter(context.Background(), category, name, model, class, location, color, year, price, square, salary, distance, hours)
	if err != nil {
		log.Printf("|| %v", err)
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}
	c.JSON(200, f)
}

// GetHouses
// @Summary      House
// @Description
// @Tags         house
// @Accept       json
// @Produce      json
// @Success      200 {object} []swagger_object.House
// @Failure      400
// @Failure      500
// @Router       /houses [GET]
func (s Server) GetHouses(c *gin.Context) {
	houses, err := s.service.GetHouses(context.Background())
	if err != nil {
		c.JSON(500, err)
		return
	}
	c.JSON(200, houses)
}

// GetHouse
// @Summary      House
// @Description
// @Tags         house
// @Accept       json
// @Produce      json
// @Param        houseId query string true "HouseGet"
// @Success      200 {object} swagger_object.House
// @Failure      400
// @Failure      500
// @Router       /house [GET]
func (s Server) GetHouse(c *gin.Context) {
	houseId := c.Query("houseId")
	house, err := s.service.GetHouse(context.Background(), houseId)
	if err != nil {
		c.JSON(500, err)
		return
	}
	c.JSON(200, house)
}

// RegisterHouse
// @Summary      House
// @Description
// @Tags         house
// @Accept       json
// @Produce      json
// @Param        house body swagger_object.House true "HouseRegister"
// @Success      200 {object} string
// @Failure      400
// @Failure      500
// @Router       /register/house [POST]
func (s Server) RegisterHouse(c *gin.Context) {
	var house swagger_object.House
	if err := c.ShouldBind(&house); err != nil {
		c.JSON(400, err)
		return
	}
	if house.Category != "House" {
		c.JSON(400, gin.H{
			"error": "invalid category",
		})
		return
	}
	houseId, err := s.service.CreateHouse(context.Background(), house)
	if err != nil {
		c.JSON(500, err)
		return
	}

	c.JSON(200, houseId)
}

// UpdateCar
// @Summary      Car
// @Description
// @Tags         car
// @Accept       json
// @Produce      json
// @Param        newCar body entity.Car true "CarUpdate"
// @Success      200
// @Failure      400
// @Failure      500
// @Router       /update/car [PUT]
func (s Server) UpdateCar(c *gin.Context) {
	var newCar entity.Car
	if err := c.ShouldBindJSON(&newCar); err != nil {
		c.JSON(400, err)
		return
	}
	if err := s.service.UpdateCar(context.Background(), newCar); err != nil {
		log.Println("1: ", err)
		c.JSON(500, err)
		return
	}

	c.JSON(200, gin.H{
		"ok": "updated",
	})
}

// GetCars
// @Summary      Car
// @Description
// @Tags         car
// @Accept       json
// @Produce      json
// @Success      200 {object} []swagger_object.Car
// @Failure      400
// @Failure      500
// @Router       /cars [GET]
func (s Server) GetCars(c *gin.Context) {
	cars, err := s.service.GetCars(context.Background())
	if err != nil {
		c.JSON(500, err)
	}
	c.JSON(200, cars)
}

// GetCar
// @Summary      Car
// @Description
// @Tags         car
// @Accept       json
// @Produce      json
// @Param        carId path string true "CarGet"
// @Success      200 {object} swagger_object.Car
// @Failure      400
// @Failure      500
// @Router       /car/{carId} [GET]
func (s Server) GetCar(c *gin.Context) {
	carId := c.Param("carId")

	car, err := s.service.GetCar(context.Background(), carId)
	if err != nil {
		c.JSON(500, err)
		log.Println(err)
		return
	}

	c.JSON(200, car)
}

// RegisterCar
// @Summary      Car
// @Description
// @Tags         car
// @Accept       json
// @Produce      json
// @Param        request body swagger_object.Car true  "CarRegister"
// @Success      200 {object} string
// @Failure      400
// @Failure      500
// @Router       /register/car [POST]
func (s Server) RegisterCar(c *gin.Context) {
	var car swagger_object.Car
	if err := c.ShouldBindJSON(&car); err != nil {
		c.JSON(400, err)
		return
	}
	carId, err := s.service.CreateCar(context.Background(), car)
	if err != nil {
		c.JSON(500, fmt.Sprintf("%v", err))
		log.Println(err)
		return
	}

	c.JSON(200, carId)
}

func invalidInput(c *gin.Context, err error) bool {
	if err != nil {
		c.JSON(400, gin.H{
			"error": "invalid input for type integer",
		})
		return true
	}
	return false
}
