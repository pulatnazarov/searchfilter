package entity

import (
	"filter/api/swagger_object"
	"github.com/google/uuid"
)

type Filter struct {
	Cars   []Car
	Houses []House
	Jobs   []Job
}

type Job struct {
	Id       string
	Category string
	Class    string
	Location string
	Salary   int
	Hours    int
}

func NewJob(job swagger_object.Job) Job {
	return Job{
		Id:       uuid.NewString(),
		Category: job.Category,
		Class:    job.Class,
		Location: job.Location,
		Salary:   job.Salary,
		Hours:    job.Hours,
	}
}

type House struct {
	Id       string
	Category string
	Class    string
	Location string
	Year     int
	Price    int
	Square   int
}

func NewHouse(house swagger_object.House) House {
	return House{
		Id:       uuid.NewString(),
		Category: house.Category,
		Class:    house.Class,
		Location: house.Location,
		Year:     house.Year,
		Price:    house.Price,
		Square:   house.Square,
	}
}

type Car struct {
	Id       string
	Category string
	Name     string
	Model    string
	Color    string
	Year     int
	Price    int
	Distance int
}

func NewCar(car swagger_object.Car) Car {
	return Car{
		Id:       uuid.NewString(),
		Category: car.Category,
		Name:     car.Name,
		Model:    car.Model,
		Color:    car.Color,
		Year:     car.Year,
		Price:    car.Price,
		Distance: car.Distance,
	}
}
