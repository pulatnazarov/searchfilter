package service

import (
	"context"
	"filter/api/swagger_object"
	"filter/entity"
	"filter/postgres"
	"log"
)

type Service struct {
	repo postgres.PostgresRepo
}

func New(repo postgres.PostgresRepo) Service {
	return Service{
		repo: repo,
	}
}

type ServiceRepo interface {
	Filter(ctx context.Context, category, name, model, class, location, color string,
		year, price, square, salary, distance, hours int) (entity.Filter, error)

	GetHouses(ctx context.Context) ([]entity.House, error)
	GetHouse(ctx context.Context, houseId string) (entity.House, error)
	CreateHouse(ctx context.Context, house swagger_object.House) (string, error)

	UpdateCar(ctx context.Context, newCar entity.Car) error
	GetCars(ctx context.Context) ([]entity.Car, error)
	GetCar(ctx context.Context, carId string) (entity.Car, error)
	CreateCar(ctx context.Context, car swagger_object.Car) (string, error)
}

func (s Service) Filter(ctx context.Context, category, name, model, class, location, color string,
	year, price, square, salary, distance, hours int) (entity.Filter, error) {
	f, err := s.repo.Filter(ctx, category, name, model, class, location, color, year, price, square, salary, distance, hours)
	if err != nil {
		return entity.Filter{}, err
	}
	return f, nil
}

func (s Service) GetHouses(ctx context.Context) ([]entity.House, error) {
	houses, err := s.repo.SelectHouses(ctx)
	if err != nil {
		return nil, err
	}
	return houses, nil
}

func (s Service) GetHouse(ctx context.Context, houseId string) (entity.House, error) {
	house, err := s.repo.SelectHouse(ctx, houseId)
	if err != nil {
		return entity.House{}, err
	}
	return house, nil
}

func (s Service) CreateHouse(ctx context.Context, house swagger_object.House) (string, error) {
	newHouse := entity.NewHouse(house)
	if err := s.repo.InsertHouse(ctx, newHouse); err != nil {
		return "", err
	}
	return newHouse.Id, nil
}

func (s Service) UpdateCar(ctx context.Context, newCar entity.Car) error {
	err := s.repo.UpdateCar(ctx, newCar)
	log.Println("2: ", err)
	return err
}

func (s Service) GetCars(ctx context.Context) ([]entity.Car, error) {
	cars, err := s.repo.SelectCars(ctx)
	if err != nil {
		return nil, err
	}
	return cars, nil
}

func (s Service) GetCar(ctx context.Context, carId string) (entity.Car, error) {
	car, err := s.repo.SelectCar(ctx, carId)
	if err != nil {
		log.Println("1: ", err)
		return entity.Car{}, err
	}
	return car, nil
}

func (s Service) CreateCar(ctx context.Context, car swagger_object.Car) (string, error) {
	newCar := entity.NewCar(car)
	if err := s.repo.InsertCar(ctx, newCar); err != nil {
		return "", err
	}
	return newCar.Id, nil
}
